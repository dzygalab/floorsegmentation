import numpy as np
import cv2
from skimage.segmentation import slic
from skimage import io
import skimage.segmentation as seg
import skimage.filters as filters
import skimage.color as color
from skimage.future import graph
from skimage import feature, transform
from skimage import measure
from skimage import exposure	
from skimage import img_as_float
from skimage.feature import local_binary_pattern
import os
import matplotlib.pyplot as plt

# Plot adjusments
font = {'family': 'serif',
		'color':  'darkred',
		'weight': 'normal',
		'size': 16,
		}

def show_image(image):
    plt.figure(figsize=(8, 12))
    plt.imshow(image)
    plt.axis('off')
    plt.show()

def read_ground_truth(gt_name):
	"""Read a ground truth image and create a binary mask"""
	img_gt = io.imread(gt_name)
	img_gt = img_gt[:,:,0]
	img_gt[img_gt <= 125] = 0
	img_gt[img_gt > 125] = 1
	return img_gt

def image_resize(image):
	"""Resize image depending on original size"""
	imh, imw, _ = image.shape
	if imh > 600 and imh < 1000: 
		resized = transform.resize(image, (int(imh/2), int(imw/2)))
		print('Image resized to ', resized.shape)
	elif imh > 1000:
		resized = transform.resize(image, (int(imh/4), int(imw/4)))
		print('Image resized to ', resized.shape)
	else:
		resized = image
	return resized

def slic_segmentation(image, num_segmnts = 300, compactness = 7):
	"""Generate slic labels dependin on number of initial segments and compactness"""
	labels = slic(image, n_segments = num_segmnts, sigma = 5, compactness = 7)
	return labels

def lbp_filter(simg, vimg):
    """create edge map usin local binary pattern 'wiht' var method"""
    METHOD = 'var'
    radius = 3
    n_points = 8 * radius
	
    lbp2 = local_binary_pattern(simg, n_points, radius, METHOD)
    lbp2[np.isnan(lbp2)] = np.nanmean(lbp2)
    lbp3 = local_binary_pattern(vimg, n_points, radius, METHOD)
    lbp3[np.isnan(lbp3)] = np.nanmean(lbp3)
    lbp = lbp3 + lbp2
    x = 5
    lbp[:, :x] = np.median(lbp)
    lbp[:x, :] = np.median(lbp)
    lbp[-x:, :] = np.median(lbp)
    lbp[:, -x:] = np.median(lbp)
    
	# lbp = local_binary_pattern(filters.gaussian(image,sigma=5), n_points, radius, METHOD)
    return lbp

def weight_boundary(graph, src, dst, n):
    """util for hierarchical merge"""
    default = {'weight': 0.0, 'count': 0}

    count_src = graph[src].get(n, default)['count']
    count_dst = graph[dst].get(n, default)['count']

    weight_src = graph[src].get(n, default)['weight']
    weight_dst = graph[dst].get(n, default)['weight']

    count = count_src + count_dst
    return {
        'count': count,
        'weight': (count_src * weight_src + count_dst * weight_dst)/count
    }

def merge_boundary(graph, src, dst):
    """util for hierarchical merge"""
    pass

def floor_label(labels):
	"""takes the most weighted lable from the central lower part of image"""
	h, w = labels.shape
	potential_floor = labels[int(h/2):, int(w/4):int(w*3/4)]
    # choose the biggest segments
	unique, counts = np.unique(potential_floor, return_counts=True)
	res_dict = dict(zip(unique, counts))
	return sorted(res_dict.items(), key=lambda kv: kv[1], reverse=True)[0][0]

def iou_evaluation(mask, img_gt):
    """evaluate IoU of labeld image and ground truth"""
    return np.logical_and(mask, img_gt).sum() / np.logical_or(mask, img_gt).sum()

def masking_image(img, mask, *argv): #img_gt = None):
	"""apply lable mask on image and in case of evaluation apply ground truth mask """
	if len(argv) == 0:
		dst = img.copy()
		dst[:,:,1] = dst[:,:,1] * (1-mask)	
	else: 
		dst = img.copy()
		dst[:,:,1] = dst[:,:,1] * (1-mask)
		dst[:,:,0] = dst[:,:,0] * (1-argv[0])	
	return dst

def smooth_contour(mask):
	"""approximate shape of mask with contours """
	contours, _ = cv2.findContours(cv2.convertScaleAbs(mask),cv2.RETR_CCOMP,cv2.CHAIN_APPROX_NONE) 
	mask_interp = np.zeros_like(mask)
	areas = []
	for cnt in contours:
		areas.append(cv2.contourArea(cnt))    
	true_contour = contours[np.argmax(areas)]
	epsilon = 0.01*cv2.arcLength(true_contour, True)
	approx = cv2.approxPolyDP(true_contour, epsilon, True)
	cv2.fillPoly(mask_interp, [approx], color=(255,255,255))
	mask_interp = mask_interp / 255
	return mask_interp

def weight_histogram(g_edg, percent_thresh = 0.80):
    """plot histogram of RAG edges weights and return threshold for a given percentile"""
    from scipy.stats import percentileofscore
    w_list = list(g_edg.edges(data='weight'))
    weights = np.asarray([w[2] for w in w_list])
    percentile = np.quantile(weights, percent_thresh)
    return percentile

def mask_from_merging(labels, edges, percentile):
	"""creates graph from labels and edge_map and merges with threshold from give percentile """
	g_edg = graph.rag_boundary(labels, edges)
	hierarchical_tresh = weight_histogram(g_edg, percentile)
	labels_merged = graph.merge_hierarchical(labels, g_edg, thresh=hierarchical_tresh, rag_copy=True,
                                             in_place_merge=True,
                                             merge_func=merge_boundary,
                                             weight_func=weight_boundary)

	target_label = floor_label(labels_merged)
	mask = np.zeros(labels_merged.shape)
	mask[labels_merged == target_label] = 1
	return mask 
	

# ------- Main pipeline function -------

def floor_segmentation_pipeline(file):
	"""main pipeline script for one file"""

	# 0. Read and preprocess image 

	# Load and resize image 
	img = io.imread(file)  
	imh_orig, imw_orig, _ = img.shape
	img_original = img.copy() # save to aplly mask in the end 
	img = image_resize(img)
	
	# convert images to other colorspaces 
	hsvimg = color.rgb2hsv(img)
	simg = hsvimg[:,:,1]
	vimg = hsvimg[:,:,2]
	img_adapteq = exposure.equalize_adapthist(img, clip_limit=0.5)
	
	# 1. Voting 

	# SLIC
	num_segmnts = 300
	labels_list = []
	labels_list.append(slic_segmentation(img, num_segmnts))
	labels_list.append(slic_segmentation(hsvimg, num_segmnts))
	labels_list.append(slic_segmentation(img_adapteq, num_segmnts))

	# Edge maps 
	edge_list = []
	edge_list.append(filters.sobel(simg))
	lbp = lbp_filter(simg, vimg)
	edge_list.append(lbp)

	# thresholds for hierarchucal merging 
	percentile_lsit = [.75, .80, .85]

	# voting process
	masks_voting = np.zeros(simg.shape)
	for labels in labels_list:
		for edges in edge_list:
			for percentile in percentile_lsit: 
				mask = mask_from_merging(labels, edges, percentile)
				masks_voting = masks_voting + mask

	N = len(labels_list) * len(edge_list) * len(percentile_lsit)
	if masks_voting.shape[0] != imh_orig:
		masks_voting = transform.resize(masks_voting, (imh_orig, imw_orig))
	mask_sum = np.copy(masks_voting) 
	eps = 1/2 # voting threshold
	masks_voting[masks_voting <= N*eps] = 0
	masks_voting[masks_voting > N*eps] = 1

	# _, ax = plt.subplots(1,3, figsize = (12,8))
	# ax[0].imshow(img), ax[0].axis('off')
	# ax[1].imshow(mask_sum), ax[1].axis('off')
	# ax[2].imshow(masks_voting), ax[2].axis('off')
	# plt.savefig(res_dir + file, dpi=150)
	# plt.show()

	return img_original, masks_voting


if __name__ == "__main__":

	# Many files
	working_tree = '/home/dzyga/My/Python/ITJim/Projects/FloorSegmentation/'
	dataset_dir = working_tree + 'dataset/' # -> dataset is here
	gt_dir = dataset_dir + 'ground_truth/' # -> find truth here 

	# if you want custom masks
	mask_dir = working_tree +'v2/'# 'joined/' # 'fastfcn_seg1/' #  ## 
	
	# the same result directory name in both cases of evaluating and full datasets 
	res_dir = working_tree + 'result_images/' # -> save here
	res_dir_mask = res_dir + 'masks/' # -> binary masks save here 

	if not os.path.exists(res_dir):
		os.makedirs(res_dir)

	if not os.path.exists(res_dir_mask):
		os.makedirs(res_dir_mask)

	evaluation = True
	print(' ')

	if evaluation:
		print('Evaluating results wiht ground truth photos')
		iou = []
		for file in os.listdir(gt_dir):
			if file.endswith('.jpg'):
				
				img_gt  = read_ground_truth(gt_dir + file)

				# ---- segmentation function ----
				img, mask = floor_segmentation_pipeline(dataset_dir+file)
				
				# ----- custom mask -----
				# img = io.imread(dataset_dir + file)  

				mask_cust = io.imread(mask_dir + file[:-4] + '.png')
				if len(mask_cust.shape) > 2:
					mask_cust = mask_cust[:,:,0]
				mask_cust[mask_cust>127] = 1

				# If you want to postprocess custom masks
				# mask = mask_cust # !!!! 

				# One mask comparing 
				# from texture_features_postproc import one_mask_postproc
				# mask_post = one_mask_postproc(cv2.cvtColor(img, cv2.COLOR_BGR2HSV)[:,:,1], mask)

				# Two masks comparing
				# # New version
				from texture_features_postproc import two_mask_postproc
				mask_post = two_mask_postproc(cv2.cvtColor(img, cv2.COLOR_BGR2HSV)[:,:,1], mask, mask_cust)

				# Old version	
				# from masks_postprocessing import two_mask_postproc
				# mask_post = two_mask_postproc(cv2.cvtColor(img, cv2.COLOR_BGR2HSV)[:,:,1], mask, mask_cust)

				# fig, ax = plt.subplots(1,4,figsize = (6,8))
				# ax[0].imshow(img), ax[0].set_title('Source'), ax[0].axis('off')
				# ax[1].imshow(mask_cust), ax[1].set_title('CNN'), ax[1].axis('off')
				# ax[2].imshow(mask), ax[2].set_title('Classical'), ax[2].axis('off')
				# ax[3].imshow(mask_post), ax[3].set_title('Post processing'), ax[3].axis('off')
				# plt.savefig(res_dir + 'fig_' + file, dpi=150)
				# plt.show()
				
				# evaluate
				iou_img = iou_evaluation(mask_post, img_gt)
				print('iou is ', iou_img)
				iou.append(iou_img)

				# show results 			
				result_img = masking_image(img, mask, img_gt)	
				cv2.putText(result_img, 'IoU is {:.4f}'.format(iou_img), 
							(50,100), cv2.FONT_HERSHEY_SIMPLEX, 3, (255,0,0), 4, cv2.LINE_AA)

				# save result 
				io.imsave(res_dir + file, result_img) # masked image 
				mask[mask==1] = 255 # from 0/1 to 0/255
				io.imsave(res_dir_mask + file[:-4] + '.png', mask.astype('int')) # binary mask
				
				print(' ')
			
		meanIoU = np.mean(np.asarray(iou))
		print('Mean IoU is ', meanIoU)
			
	else:
		print('Processing full dataset')
		for file in os.listdir(dataset_dir):
			if file.endswith('.jpg'):
				
				# ---- segmentation function ----
				img, mask = floor_segmentation_pipeline(dataset_dir+file)
			
				# show results 			
				result_img = masking_image(img, mask)	
				
				# save result - the same folder as for evaluation results! 
				io.imsave(res_dir + file, result_img) # masked image 
				mask[mask==1] = 255 # from 0/1 to 0/255
				io.imsave(res_dir_mask + file[:-4] + '.png', mask.astype('int')) # binary mask
				
				print(' ')