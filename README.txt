9 March 2020
By Anastasiia S.

This repository is a part of a Floor Segmentation project in ITJim. 

In this part classiacal approaches for segmentation are used. They are SLIC superpixaligh, RAG (graph) merging and segmentation mask postprocessing using texture features. 

The repository contains:
- dataset 
- result images 
- python codes 
- ipynb notebooks with reasearching process
- reports


