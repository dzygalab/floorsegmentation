import numpy as np
import cv2
from skimage.segmentation import slic
from skimage import io
import skimage.segmentation as seg
import skimage.filters as filters
import skimage.color as color
from skimage.future import graph
from skimage import feature, transform
from skimage import measure
from skimage import exposure	
from skimage.feature import local_binary_pattern

import os

import matplotlib.pyplot as plt

# ---- PREPROCESSING ----

def write_result(image, name, imwrite_result = False):
	if imwrite_result: 
		io.imsave(res_dir + name + '.jpg', image)

def read_ground_truth(gt_name):
	img_gt = io.imread(gt_name)
	img_gt = img_gt[:,:,0]
	img_gt[img_gt <= 125] = 0
	img_gt[img_gt > 125] = 1
	return img_gt

def image_resize(img):
	imh, imw, imc = img.shape
	if imh > 600 and imh < 1000: 
		resized = cv2.resize(img, None, fx=0.5,fy=0.5)
		print('Image shape resized to ', img.shape)
		imh, imw, imc = resized.shape
	elif imh > 1000:
		resized = cv2.resize(img, None, fx=0.25,fy=0.25)
		print('Image shape resized to ', img.shape)
		imh, imw, imc = resized.shape
	else:
		resized = img
	
	return resized


# ---- SEGMENTATION ----

def slic_segmentation(image, num_segmnts = 300, compactness = 7):
	labels = slic(image, n_segments = num_segmnts, sigma = 5, compactness = compactness)
	segments = color.label2rgb(labels, image, kind='avg')
	write_result(segments, 'slic_labels')
	return labels

def lbp_filter(image, simg=None, vimg=None):
    METHOD = 'var'
    radius = 3
    n_points = 8 * radius
    # lbp = local_binary_pattern(image, n_points, radius, METHOD)
    # lbp = local_binary_pattern(filters.gaussian(image,sigma=5), n_points, radius, METHOD)
    lbp1 = local_binary_pattern(simg, n_points, radius, METHOD)
    lbp2 = local_binary_pattern(vimg, n_points, radius, METHOD)
    lbp = lbp1+lbp2
    x = 5
    lbp[:, :x] = np.median(lbp)
    lbp[:x, :] = np.median(lbp)
    lbp[-x:, :] = np.median(lbp)
    lbp[:, -x:] = np.median(lbp)
    # Handle missing value
    lbp[np.isnan(lbp)] = np.nanmean(lbp)
    return lbp

def weight_boundary(graph, src, dst, n):
    default = {'weight': 0.0, 'count': 0}

    count_src = graph[src].get(n, default)['count']
    count_dst = graph[dst].get(n, default)['count']

    weight_src = graph[src].get(n, default)['weight']
    weight_dst = graph[dst].get(n, default)['weight']

    count = count_src + count_dst
    return {
        'count': count,
        'weight': (count_src * weight_src + count_dst * weight_dst)/count
    }

def merge_boundary(graph, src, dst):
    pass

def hierarchical_merge(labels, g_edg, percent_rate):
    Q = weight_histogram(g_edg, percent_rate)
    print('---------', Q)
    labels_hier_slic = graph.merge_hierarchical(labels, g_edg, Q, rag_copy=True,
                                    in_place_merge=True,
                                    merge_func=merge_boundary,
                                    weight_func=weight_boundary)

    target_label = floor_label(labels_hier_slic)
    mask = np.zeros(labels_hier_slic.shape)
    mask[labels_hier_slic == target_label] = 1
    return mask 

def weight_histogram(g_edg, percent_thresh = 0.80):
    from scipy.stats import percentileofscore
    w_list = list(g_edg.edges(data='weight'))
    weights = np.asarray([w[2] for w in w_list])
    percentile = np.quantile(weights, percent_thresh)
    return percentile

def thresh_iterate(labels, g_edg):
    for score in np.linspace(0.65, 0.95, 10):
        Q = weight_histogram(g_edg, percent_thresh = score)
        print('Percentile of a score {} is {}'.format(score, Q))
        mask = hierarchical_merge(labels, g_edg, score)
        approx = interpolated_shape(mask)
        # subplots_image({'Original':img, 'approximation':approx})
        #show_mask(mask, approx)
        mask_interp = np.zeros_like(mask)
        cv2.fillPoly(mask_interp, [approx], color=(255,255,255))
        show_image(mask_interp, title='Interpolated with {} angles'.format(len(approx)))
        print('Num of angles is ', len(approx))
        num_of_angles = 30
        if len(approx) < num_of_angles:
              break
    return approx

# ---- POSTPROCESSING ----

def smooth_contour(mask):
	contours, _ = cv2.findContours(cv2.convertScaleAbs(mask),cv2.RETR_CCOMP,cv2.CHAIN_APPROX_NONE) 
	mask_interp = np.zeros_like(mask)
	areas = []
	for cnt in contours:
		areas.append(cv2.contourArea(cnt))    
	true_contour = contours[np.argmax(areas)]
	epsilon = 0.01*cv2.arcLength(true_contour, True)
	approx = cv2.approxPolyDP(true_contour, epsilon, True)
	cv2.fillPoly(mask_interp, [approx], color=(255,255,255))
	mask_interp = mask_interp / 255
	return mask_interp

def interpolated_shape(mask):
    contours, _ = cv2.findContours(cv2.convertScaleAbs(mask),cv2.RETR_CCOMP,cv2.CHAIN_APPROX_NONE)
    true_contour = max(contours, key=cv2.contourArea) 
    epsilon = 0.005*cv2.arcLength(true_contour, True)
    approx = cv2.approxPolyDP(true_contour, epsilon, True)
    return approx


# ----- LABELING AND EVALUATION ----- 

def floor_label(labels):
	"""takes the most weighted lable from the central lower part of image"""
	h, w = labels.shape
	potential_floor = labels[int(h/2):, int(w/4):int(w*3/4)]
    # choose the biggest segments
	unique, counts = np.unique(potential_floor, return_counts=True)
	res_dict = dict(zip(unique, counts))
	return sorted(res_dict.items(), key=lambda kv: kv[1], reverse=True)[0][0]

def masking_image(img, mask, *argv): #img_gt = None):
	if len(argv) == 0:
		dst = img.copy()
		dst[:,:,1] = dst[:,:,1] * (1-mask)	
	else: 
		dst = img.copy()
		dst[:,:,1] = dst[:,:,1] * (1-mask)
		dst[:,:,0] = dst[:,:,0] * (1-argv[0])	
	return dst

def iou_evaluation(mask, img_gt):
    return np.logical_and(mask, img_gt).sum() / np.logical_or(mask, img_gt).sum()


#  ---- VISUALIZATION ----

def show_image(image, title = ''):
    plt.figure(figsize=(8, 12))
    plt.imshow(image)
    plt.axis('off')
    plt.title(title)
    plt.show()

def subplots_image(img_dict):  
    """from {'title': image} dictionary create a subplots
    Ex.: subplots_image('rgb' = img, 'hsv' = hsvimg, 'mask'= mask)  """
    N = len(img_dict)
    fig, ax = plt.subplots(1, N, figsize = (6, 10))
    i = 0
    for text, image in img_dict.items(): 
        ax[i].imshow(image)
        ax[i].set_title(text)
        ax[i].axis('off');
        i+=1
    plt.show()

def show_mask(mask_baseline, approx):
    mask_interp = np.zeros_like(mask_baseline)
    cv2.fillPoly(mask_interp, [approx], color=(255,255,255))
    _, ax = plt.subplots(1,2, figsize = (8,12))
    ax[0].imshow(mask_baseline)
    ax[0].set_title('Before')
    ax[0].axis('off')
    ax[1].imshow(mask_interp)
    ax[1].set_title('Interpolated with {} angles'.format(len(approx)))
    ax[1].axis('off')
    plt.show()


    # def comparing_images(file, masking_image):
# 	dir_ref = working_tree + 'result_images_iou6955/'
# 	if not os.path.exists(res_dir + 'comparing/'):
# 			os.makedirs(res_dir + 'comparing/')
# 	ref_img = io.imread(dir_ref + file)
# 	im_stack = np.hstack((ref_img, masking_image)) #stacking images side-by-side
# 	h, w, c = im_stack.shape

# 	font = cv2.FONT_HERSHEY_SIMPLEX 
# 	fontScale = 3
# 	text_color = (255, 0, 0) 
# 	thickness = 6

# 	text1 = 'before'
# 	org1 = (int(w/5), int(h/4)) 
# 	cv2.putText(im_stack, text1, org1, font,  
# 					fontScale, text_color, thickness, cv2.LINE_AA)
# 	text2 = 'after'
# 	org2 = (int(3*w/4), int(h/4)) 
# 	cv2.putText(im_stack, text2, org2, font,  
# 					fontScale, text_color, thickness, cv2.LINE_AA); 
# 	io.imsave(res_dir + 'comparing/' + file, im_stack)