"""This is a script for mask segmentation post processing 
analyzing texure features of the different segments
27 Feb 2020 By N.
"""

import cv2
import numpy as np
from skimage.measure import regionprops
from skimage.feature import greycomatrix, greycoprops
from scipy.spatial.distance import pdist

import matplotlib.pyplot as plt

def sorted_contours(segmentation_mask):
    contours, _ = cv2.findContours(cv2.convertScaleAbs(segmentation_mask),
                                        cv2.RETR_CCOMP,
                                        cv2.CHAIN_APPROX_NONE)
    return sorted(contours, key = cv2.contourArea,reverse=True)

def contours_to_labels(segmentation_mask):
    contours, _ = cv2.findContours(cv2.convertScaleAbs(segmentation_mask),
                                        cv2.RETR_CCOMP, #cv2.RETR_TREE, # 
                                        cv2.CHAIN_APPROX_SIMPLE)
    contours = sorted(contours, key = cv2.contourArea,reverse=True)
    map_labels = np.zeros_like(segmentation_mask)
    for cnt in contours:
        c = np.random.randint(1, 254) # 0 and 255 are reserved for non floor and floor;
        cv2.fillPoly(map_labels, [cnt], color = c) 
    
    map_labels = cv2.UMat(map_labels).get()
    return map_labels # uint 0:255

# def clean_contours(contours_list):
#     # calculate centroids of contours and kill contour if centroid is close to some one
#     centr = []
#     for i, cnt in enumerate(contours_list):
#         M = cv2.moments(cnt)
#         if M["m00"] != 0:
#             x = M["m10"] / M["m00"]
#             y = M["m01"] / M["m00"]
#         else:
#             x = 0
#             y = 0
#         centr.append((x,y))

#     # compare centroids to kill double contours
#     true_contours = contours_list.copy()

#     for i, cnt in enumerate(contours_list):
#         M = cv2.moments(cnt)
#         if M["m00"] != 0:
#             x = M["m10"] / M["m00"]
#             y = M["m01"] / M["m00"]
#         else:
#             x, y = 0, 0
        
#         for j in range(i+1,len(centr)):
#             x1 = centr[j][0]
#             y1 = centr[j][1]
#             d = (np.sqrt((x-x1)**2+(y-y1)**2))
#             if d < 10:
#                 try: 
#                     true_contours.remove(cnt)
#                 except:
#                     print('Already done')
#     return true_contours

def centroid(contour):
    M = cv2.moments(contour)
    if M["m00"] != 0:
        x = M["m10"] / M["m00"]
        y = M["m01"] / M["m00"]
    else:
        print('Error: division by zero')
        x, y = 0, 0
    return x, y 

def dist(contour, clean_contour):
    x, y = centroid(contour)
    x1, y1 =centroid(clean_contour)
    return np.sqrt((x-x1)**2+(y-y1)**2)

def clean_contours(contours_list, eps):
    true_contours = []
    for contour in contours_list:
        if cv2.contourArea(contour) > 50: 
            add_this_contour = True
            for clean_contour in true_contours:
                if dist(contour, clean_contour) < eps:
                    add_this_contour = False
                    break
            if add_this_contour and cv2.contourArea(contour)>50:
                true_contours.append(contour)
    return sorted(true_contours, key = cv2.contourArea, reverse=True)

def get_glcm_features(patch):
    """fet glcm feature for the input patch of an image
    """
    patch = cv2.convertScaleAbs(patch)
    # list of GLCM feature 
    features = [
     'dissimilarity', 
     'homogeneity', 
     'energy',
     'contrast',
     'ASM']

    N = len(features)
    feature_vec = np.zeros(N)
    glcm_img = greycomatrix(patch, [5], [0, np.pi/4, np.pi/2, 3*np.pi/4],
                            levels=256, normed=True)
    for j,f in enumerate(features):
        feature_vec[j] = greycoprops(glcm_img, f)[0,0]
    return feature_vec.reshape(1,-1)

def sorted_mask_segments(image, mask):
    map_sum = segments_from_mask(mask) 

    # create new labeled image 
    segments_postproc = regionprops(map_sum, image)
    return sorted(segments_postproc, key=lambda s: s.area)

def segments_from_mask(mask):
    map_orig = contours_to_labels(mask)
    map_inv = contours_to_labels(np.max(mask)-mask)
    map_sum = map_orig.astype('float') + map_inv.astype('float')
    return cv2.convertScaleAbs(map_sum) # uint 0:255

def find_all_contours(mask):
    # origing mask
    contours_orig = sorted_contours(mask) 
    # invered mask
    contours_inv = sorted_contours(np.max(mask)-mask)
   
    # concantenate contours and fill with colour
    all_contours = contours_orig + contours_inv
    return sorted(all_contours, key = cv2.contourArea, reverse=True)

def find_regions_of_mask(image, mask):
    # find contours of original and inverted mask
    all_contours = find_all_contours(mask)

    # kill double contours
    eps = mask.size/100000
    true_contours = clean_contours(all_contours, eps)

    map_sum = np.ones((mask.shape))
    for i,cnt in enumerate(true_contours):
        # c = np.random.randint(1,254) # random colours
        c = i*2 + 4
        cv2.fillPoly(map_sum, [cnt], color = c)        

    map_sum = cv2.convertScaleAbs(map_sum)
    # map_sum = cv2.normalize(map_sum,None,1,254,cv2.NORM_MINMAX,cv2.CV_8U)

    # create new labeled image 
    segments_postproc = regionprops(map_sum, image)

    # sort segments from low to high area 
    segments_postproc = sorted(segments_postproc, key=lambda s: s.area)

    return segments_postproc



# ----- One mask -----
def one_mask_postproc(image, mask):
    """ Obtain labels of the separate regions of a maks

    """
    segments_postproc = find_regions_of_mask(image, mask)
    
    # # find contours of original and inverted mask
    # all_contours = find_all_contours(mask)

    # # kill double contours
    # eps = mask.size/100000
    # true_contours = clean_contours(all_contours, eps)

    # map_sum = np.ones((mask.shape))
    # for i,cnt in enumerate(true_contours):
    #     # c = np.random.randint(1,254) # random colours
    #     c = i*2 + 4
    #     cv2.fillPoly(map_sum, [cnt], color = c)        

    # map_sum = cv2.convertScaleAbs(map_sum)
    # # map_sum = cv2.normalize(map_sum,None,1,254,cv2.NORM_MINMAX,cv2.CV_8U)

    # # create new labeled image 
    # segments_postproc = regionprops(map_sum, image)

    # # sort segments from low to high area 
    # segments_postproc = sorted(segments_postproc, key=lambda s: s.area)

    # --- Check!---
    # plt.imshow(map_sum), plt.title('Map sum'), plt.axis('off')
    # for props in segments_postproc:
    #     cx, cy = props.centroid  # centroid coordinates
    #     plt.text(int(cy), int(cx), props.label, fontsize=10, color='red')
    # # plt.show()
    # direct = '/home/dzyga/My/Python/ITJim/Projects/FloorSegmentation_7468/floorsegmentation/check_res/'
    # file = '{}'.format(np.random.randint(100))
    # from skimage import io
    # io.imsave(direct + file + '.png', map_sum.astype('int')) # binary mask

    # fig, ax = plt.subplots(1,3,figsize = (6,8))
    # ax[0].imshow(map_orig), ax[0].set_title('origing'), ax[0].axis('off')
    # ax[1].imshow(map_inv), ax[1].set_title('invert'), ax[1].axis('off')
    # ax[2].imshow(map_sum), ax[2].set_title('sum'), ax[2].axis('off')
    # plt.show()


    # take two most bigger segments - they are a floor and non floor areas
    bb = segments_postproc[-1].bbox
    check_mask = mask[bb[0]:bb[2], bb[1]:bb[3]]
    lVals = (check_mask * segments_postproc[-1].image).ravel().tolist()
    check = max(map(lambda val: (lVals.count(val), val), set(lVals)))[1]

    if check == 1: # => the [-1] segment is floor
        target_floor_label = segments_postproc[-1].label
        features_floor = get_glcm_features(segments_postproc[-1].intensity_image.squeeze())
        
        target_nonfloor_label = segments_postproc[-2].label
        features_nonfloor = get_glcm_features(segments_postproc[-2].intensity_image.squeeze())
        
        print('Target label is ', target_floor_label)

    elif check == 0:  # => the [-2] segment is floor
        target_floor_label = segments_postproc[-2].label
        features_floor = get_glcm_features(segments_postproc[-2].intensity_image.squeeze())
        
        target_nonfloor_label = segments_postproc[-1].label
        features_nonfloor = get_glcm_features(segments_postproc[-1].intensity_image.squeeze())

        print('Target label is ', target_floor_label)

    else:
        print(' ------ Something is wrong -----------')

    # For all segments in new labeled image exept of the bigest two 
    # find texture features and compare them with target features of flor and non flor 
    for props in segments_postproc[:-2]:
        g = get_glcm_features(props.intensity_image.squeeze()) # current feature vector
        
        # calculate euclidean distance to the 'floor' and 'non floor' points
        floor_dist = pdist(np.concatenate((g,features_floor),axis=0), 'euclidean')
        nonfl_dist = pdist(np.concatenate((g,features_nonfloor),axis=0), 'euclidean')
        
        # see which point is closer to current segment
        # and relable segment wiht corresponding label
        if floor_dist < nonfl_dist: 
            props.label = target_floor_label
        else: 
            props.label = target_nonfloor_label

    # create new binary mask 
    mask_postproc = np.zeros(mask.shape, dtype=np.float32) 
    for props in segments_postproc:
        if props.label == target_floor_label:
            for r, c in props.coords:
                mask_postproc[r,c] = 1
        else:
            for r, c in props.coords:
                mask_postproc[r,c] = 0
    
    return mask_postproc

    
# ----- Two masks -----
def two_mask_postproc(image, mask1, mask2):
    map_sum = mask1 + mask2 # takes values: 0 - definitely not a floor, 2 - definitely is a floor, 1 - will be checked 

    mask_of_interest = np.zeros(mask1.shape)
    mask_of_interest[map_sum==1] = 1
    
    # all_contours = find_all_contours(mask_of_interest)
   
    # # kill double contours
    # eps = mask1.size/100000
    # true_contours = clean_contours(all_contours, eps)

    # map_sum = np.zeros((mask1.shape)) # ??? 
    # for i,cnt in enumerate(true_contours[1:]): # start from the second. The first segment is floor + nonfloor
    #     # c = np.random.randint(50,150) # random colours
    #     c = 255 - i*4 
    #     cv2.fillPoly(map_sum, [cnt], color = c)        

    # map_sum = cv2.convertScaleAbs(map_sum)

    # # create new labeled image 
    # segments_postproc = regionprops(map_sum, image)

    # # sort segments from low to high area 
    # segments_postproc = sorted(segments_postproc, key=lambda s: s.area)

    segments_postproc = find_regions_of_mask(image, mask_of_interest)

    # target floor/non floor segments
    mask_floor = np.zeros(mask1.shape)
    mask_floor[(mask1+mask2)==(mask1 + mask2).max()] = 1
    features_floor = get_glcm_features(mask_floor*image)
    
    mask_nonfloor = np.zeros(mask1.shape)
    mask_nonfloor[(mask1+mask2)==(mask1 + mask2).min()] = 1
    features_nonfloor = get_glcm_features(mask_nonfloor*image)

    for props in segments_postproc:
        g = get_glcm_features(props.intensity_image.squeeze()) # current feature vector

        # calculate euclidean distance to the 'floor' and 'non floor' points
        floor_dist = pdist(np.concatenate((g,features_floor),axis=0), 'euclidean')
        nonfl_dist = pdist(np.concatenate((g,features_nonfloor),axis=0), 'euclidean')
        print("""
        To floor: {}
        To non floor: {}
        """.format(floor_dist, nonfl_dist))

        # see which point is closer to current segment
        # and relable segment wiht corresponding label
        if floor_dist < nonfl_dist: 
            for r, c in props.coords:
                mask_floor[r,c] = 1

    return mask_floor

