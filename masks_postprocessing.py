""" 
function for comparing and postprocessing two masks
"""

import cv2
import numpy as np
from skimage.measure import regionprops
from skimage.feature import greycomatrix, greycoprops
from scipy.spatial.distance import pdist
import matplotlib.pyplot as plt

def contours_to_labels(segmentation_mask):
    contours, _ = cv2.findContours(cv2.convertScaleAbs(segmentation_mask),
                                        cv2.RETR_CCOMP, #cv2.RETR_TREE, # 
                                        cv2.CHAIN_APPROX_SIMPLE)
    contours = sorted(contours, key = cv2.contourArea,reverse=True)
    map_labels = np.zeros_like(segmentation_mask)
    for cnt in contours:
        c = np.random.randint(1, 254) # 0 and 255 are reserved for non floor and floor;
        cv2.fillPoly(map_labels, [cnt], color = c) 
    
    map_labels = cv2.UMat(map_labels).get()
    return map_labels # uint 0:255

def segments_from_mask(mask):
    map_orig = contours_to_labels(mask)
    map_inv = contours_to_labels(np.max(mask)-mask)
    map_sum = map_orig.astype('float') + map_inv.astype('float')

    return cv2.convertScaleAbs(map_sum) # uint 0:255

def get_glcm_features(patch):
    """fet glcm feature for the input patch of an image
    """
    patch = cv2.convertScaleAbs(patch)
    # list of GLCM feature 
    features = [
     'dissimilarity', 
     'homogeneity', 
     'energy',
#      'correlation', # return NaN sometimes
     'contrast',
     'ASM']

    N = len(features)
    feature_vec = np.zeros(N)
    glcm_img = greycomatrix(patch, [5], [0, np.pi/4, np.pi/2, 3*np.pi/4],
                            levels=256, normed=True)
    for j,f in enumerate(features):
        feature_vec[j] = greycoprops(glcm_img, f)[0,0]
    return feature_vec.reshape(1,-1)

def sorted_mask_segments(image, mask):
    map_sum = segments_from_mask(mask) 

    # create new labeled image 
    segments_postproc = regionprops(map_sum, image)
    return sorted(segments_postproc, key=lambda s: s.area)

def two_mask_postproc(image, mask1, mask2):
    """ Obtain labels of the separate regions of a maks
        masks must have 0:1 values 
    """
    map1 = segments_from_mask(mask1)
    map2 = segments_from_mask(mask2)

    map_sum = map1.astype('float') + map2.astype('float') # float 
    map_sum = cv2.convertScaleAbs(map_sum)

    # create new labeled image 
    segments_postproc = regionprops(map_sum, image)
    segments_postproc = sorted(segments_postproc, key=lambda s: s.area)

    mask_intersection = cv2.convertScaleAbs(mask1 * mask2)  # floor => 1
    mask_union = mask1.astype('float') + mask2.astype('float')
    mask_union[mask_union>0] = 1
    mask_union = cv2.convertScaleAbs(mask_union) # non floor => 0

    double_floor = sorted_mask_segments(image, mask_intersection)[-2:]
    double_nonfloor = sorted_mask_segments(image, mask_union)[-2:] 

    # floor 
    temp_mask = np.zeros(mask1.shape)
    for r, c in double_floor[0].coords:
        temp_mask[r,c] = 1
    if (temp_mask * mask_intersection).max() == 1: 
        double_floor_segm = double_floor[0]
    else:
        double_floor_segm = double_floor[1]
        
    # non floor 
    temp_mask = np.zeros(mask1.shape)
    for r, c in double_nonfloor[0].coords:
        temp_mask[r,c] = 1
    if (temp_mask*mask_union).max() == 1: 
        double_nonfloor_segm = double_nonfloor[1]   
    else:
        double_nonfloor_segm = double_nonfloor[0]
    
    # _, ax = plt.subplots(1,2,figsize = (6,8))
    # ax[0].imshow(double_floor_segm.intensity_image), ax[0].set_title('floor'), ax[0].axis('off')
    # ax[1].imshow(double_nonfloor_segm.intensity_image), ax[1].set_title('non floor'), ax[1].axis('off')
    # plt.show()

    # fill segment map with 0 and 1 corresponded to non floor and floor 
    mask_ref_floor = np.zeros(map_sum.shape)
    mask_ref_nonfloor = np.zeros(map_sum.shape)

    for r, c in double_floor_segm.coords:
        map_sum[r,c] = 255
        mask_ref_floor[r,c] = 1
    for r, c in double_nonfloor_segm.coords:
        map_sum[r,c] = 0
        mask_ref_nonfloor[r,c] = 1
    
    # _, ax = plt.subplots(1,2,figsize = (6,8))
    # ax[0].imshow(mask_ref_floor), ax[0].set_title('floor'), ax[0].axis('off')
    # ax[1].imshow(mask_ref_nonfloor), ax[1].set_title('non floor'), ax[1].axis('off')
    # plt.show()
    
    # calculate feature vector for floor and non floor
    features_floor = get_glcm_features(image*mask_ref_floor)
    features_nonfloor = get_glcm_features(image*mask_ref_nonfloor)

    for props in segments_postproc[:-2]:
        g = get_glcm_features(props.intensity_image.squeeze()) # current feature vector

        # calculate euclidean distance to the 'floor' and 'non floor' points
        floor_dist = pdist(np.concatenate((g,features_floor),axis=0), 'euclidean')
        nonfl_dist = pdist(np.concatenate((g,features_nonfloor),axis=0), 'euclidean')
        print("""
        To floor: {}
        To non floor: {}
        """.format(floor_dist, nonfl_dist))

        # see which point is closer to current segment
        # and relable segment wiht corresponding label
        if floor_dist < nonfl_dist: 
            for r, c in props.coords:
                map_sum[r,c] = 255 
        else: 
            for r, c in props.coords:
                map_sum[r,c] = 1 
    map_sum[map_sum<255]=0

    return map_sum