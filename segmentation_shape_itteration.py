import numpy as np
import cv2
from skimage.segmentation import slic
from skimage import io
import skimage.segmentation as seg
import skimage.filters as filters
import skimage.color as color
from skimage.future import graph
from skimage import feature, transform
from skimage import measure
from skimage import exposure	
from skimage.feature import local_binary_pattern
import os
import matplotlib.pyplot as plt

# My functions
from florsegment_funcs import slic_segmentation
from florsegment_funcs import image_resize
from florsegment_funcs import thresh_iterate
from florsegment_funcs import read_ground_truth
from florsegment_funcs import iou_evaluation
from florsegment_funcs import masking_image
from florsegment_funcs import hierarchical_merge
from florsegment_funcs import show_mask, show_image
from florsegment_funcs import lbp_filter
from florsegment_funcs import subplots_image


# ---------------------------------------------------------------
def floor_segmentation_pipeline(file):

	# Load and resize image 
	img = cv2.imread(file)  
	imh_orig, imw_orig, _ = img.shape
	img_original = img.copy() # save to aplly mask in the end 
	img = image_resize(img)
	
	hsvimg = color.rgb2hsv(img)
	simg = hsvimg[:,:,1]
	vimg = hsvimg[:,:,1]

		
	# SLIC performing
	num_segmnts = 300
	labels = slic_segmentation(img, num_segmnts, compactness=7)
	edges = filters.sobel(simg)
	# edge_map_for_lbp = exposure.equalize_adapthist(simg, clip_limit=0.03)
	# edges = lbp_filter(edge_map_for_lbp) # METHOD = 'var' # NaN appears
	# edges = lbp_filter(img, simg, vimg) # METHOD = 'var' # NaN appears

	
	# Graph 
	g_edg = graph.rag_boundary(labels, edges)
	# mask_base = hierarchical_merge(labels, g_edg, 0.80)

	# itterate threshold
	approx = thresh_iterate(labels, g_edg)
	# show_mask(mask_base, approx)
	# mask_interp = np.zeros_like(mask_base)
	# cv2.fillPoly(mask_interp, [approx], color=(255,255,255))
	# show_image(mask_interp, title='Interpolated with {} angles'.format(len(approx)))

	# draw mask
	mask = np.zeros_like(edges)
	mask = cv2.fillPoly(mask, [approx], color=(255,255,255))

	# reshape mask
	if mask.shape[0] != imh_orig:
		mask = transform.resize(mask, (imh_orig, imw_orig))
		mask[mask <= 0.5] = 0
		mask[mask > 0.5] = 1

	return img_original, mask

if __name__ == "__main__":

	working_tree = '/home/dzyga/My/Python/ITJim/Projects/FloorSegmentation/'
	dataset_dir = working_tree + 'dataset/'
	gt_dir = dataset_dir + 'ground_truth/'
	res_dir = working_tree + 'result_images/'

	if not os.path.exists(res_dir):
		os.makedirs(res_dir)

	evaluation = True
	show_stack_photo = False 
	print(' ')

	if evaluation:
		print('Evaluating results wiht ground truth photos')
		iou = []
		for file in os.listdir(gt_dir):
			if file.endswith('.jpg'):
				res_saveto = res_dir + file[:-5] + '/'
				
				img_gt  = read_ground_truth(gt_dir + file)

				# ---- segmentation function ----
				img, mask = floor_segmentation_pipeline(dataset_dir+file)
				
				iou_img = iou_evaluation(mask, img_gt)
				print('*** iou is ', iou_img)
				iou.append(iou_img)
				
				result_img = masking_image(img, mask, img_gt)
				# subplots_image({'Original':img, 'Result':result_img})

				# show results 				
				io.imsave(res_dir + file, result_img)
				# if show_stack_photo:
				# 	comparing_images(file, result_img)
				print(' ')
			
		meanIoU = np.mean(np.asarray(iou))
		print('Mean IoU is ', meanIoU)
		
	
	else:
		pass 
		

			


