import numpy as np
import cv2
from skimage.segmentation import slic
from skimage import io
import skimage.segmentation as seg
import skimage.filters as filters
import skimage.color as color
from skimage.future import graph
from skimage import feature, transform
from skimage import measure
from skimage import exposure	
from skimage.feature import local_binary_pattern

import timeit
import os

import matplotlib.pyplot as plt
import seaborn as sns

from florsegment_funcs import slic_segmentation
from florsegment_funcs import lbp_filter


def write_result(image, name):
	if imwrite_result: 
		io.imsave(res_dir + name + '.jpg', image)

def read_ground_truth(gt_name):
	img_gt = io.imread(gt_name)
	img_gt = img_gt[:,:,0]
	img_gt[img_gt <= 125] = 0
	img_gt[img_gt > 125] = 1
	return img_gt

def show_image(image):
    plt.figure(figsize=(8, 12))
    plt.imshow(image)
    plt.axis('off')
	# plt.show()

def image_resize(img):
	imh, imw, imc = img.shape
	# uniform resizing 
	# imh_new = 256 
	# imw_new = int(imh_new/imh * imw)
	# resized = transform.resize(img, (imh_new, imw_new))
	# print('Image shape resized from ', img.shape)
	# print('to the size ', resized.shape)
	
	if imh > 600 and imh < 1000: 
		resized = transform.resize(img, (int(imh/2), int(imw/2)))
		print('Image shape resized to ', resized.shape)
		# imh, imw, imc = img.shape
	elif imh > 1000:
		resized = transform.resize(img, (int(imh/4), int(imw/4)))
		print('Image shape resized to ', resized.shape)
		# imh, imw, imc = img.shape
	else:
		resized = img
	
	return resized

# def slic_segmentation(image, num_segmnts):
# 	start = timeit.default_timer()
# 	labels = slic(image, n_segments = num_segmnts, sigma = 5, compactness = 7)
# 	stop = timeit.default_timer()
# 	segments = color.label2rgb(labels, image, kind='avg')
# 	# print("{} sec to process".format((stop - start)))
# 	write_result(segments, 'slic_labels')
# 	return labels

# def lbp_filter(image):
#     METHOD = 'var'
#     radius = 3
#     n_points = 8 * radius
#     lbp = local_binary_pattern(filters.gaussian(image,sigma=5), n_points, radius, METHOD)
#     # Handle missing value
#     lbp[np.isnan(lbp)] = np.nanmean(lbp)
#     return lbp

def weight_boundary(graph, src, dst, n):
    default = {'weight': 0.0, 'count': 0}

    count_src = graph[src].get(n, default)['count']
    count_dst = graph[dst].get(n, default)['count']

    weight_src = graph[src].get(n, default)['weight']
    weight_dst = graph[dst].get(n, default)['weight']

    count = count_src + count_dst
    return {
        'count': count,
        'weight': (count_src * weight_src + count_dst * weight_dst)/count
    }

def merge_boundary(graph, src, dst):
    pass

def floor_label(labels):
	"""takes the most weighted lable from the central lower part of image"""
	h, w = labels.shape
	potential_floor = labels[int(h/2):, int(w/4):int(w*3/4)]
    # choose the biggest segments
	unique, counts = np.unique(potential_floor, return_counts=True)
	res_dict = dict(zip(unique, counts))
	return sorted(res_dict.items(), key=lambda kv: kv[1], reverse=True)[0][0]

def iou_evaluation(mask, img_gt):
    return np.logical_and(mask, img_gt).sum() / np.logical_or(mask, img_gt).sum()

def masking_image(img, mask, *argv): #img_gt = None):
	if len(argv) == 0:
		dst = img.copy()
		dst[:,:,1] = dst[:,:,1] * (1-mask)	
	else: 
		dst = img.copy()
		dst[:,:,1] = dst[:,:,1] * (1-mask)
		dst[:,:,0] = dst[:,:,0] * (1-argv[0])	
	return dst

def smooth_contour(mask):
	contours, _ = cv2.findContours(cv2.convertScaleAbs(mask),cv2.RETR_CCOMP,cv2.CHAIN_APPROX_NONE) 
	mask_interp = np.zeros_like(mask)
	areas = []
	for cnt in contours:
		areas.append(cv2.contourArea(cnt))    
	true_contour = contours[np.argmax(areas)]
	epsilon = 0.01*cv2.arcLength(true_contour, True)
	approx = cv2.approxPolyDP(true_contour, epsilon, True)
	cv2.fillPoly(mask_interp, [approx], color=(255,255,255))
	mask_interp = mask_interp / 255
	return mask_interp

def comparing_images(file, masking_img):
	dir_ref = working_tree + 'result_images_iou6955/'
	if not os.path.exists(res_dir + 'comparing/'):
			os.makedirs(res_dir + 'comparing/')
	ref_img = io.imread(dir_ref + file)
	im_stack = np.hstack((ref_img, masking_img)) #stacking images side-by-side
	h, w, _ = im_stack.shape

	font = cv2.FONT_HERSHEY_SIMPLEX 
	fontScale = 3
	text_color = (255, 0, 0) 
	thickness = 6

	text1 = 'before'
	org1 = (int(w/5), int(h/4)) 
	cv2.putText(im_stack, text1, org1, font,  
					fontScale, text_color, thickness, cv2.LINE_AA)
	text2 = 'after'
	org2 = (int(3*w/4), int(h/4)) 
	cv2.putText(im_stack, text2, org2, font,  
					fontScale, text_color, thickness, cv2.LINE_AA); 
	io.imsave(res_dir + 'comparing/' + file, im_stack)

def weight_histogram(g_edg, percent_thresh = 0.80):
    from scipy.stats import percentileofscore
    w_list = list(g_edg.edges(data='weight'))
    weights = np.asarray([w[2] for w in w_list])
    percentile = np.quantile(weights, percent_thresh)
    # hist = plt.hist(weights, bins = int(len(weights)/10), normed=0)
    # ax = sns.boxplot(data=weights, orient="h", palette="Set2")
    # plt.axvline(percentile, color='r')
    # plt.xlim(0,0.05)
    # fig1 = plt.gcf()
    # plt.draw()
    # fig1.savefig(res_dir + 'histograms/' + file)
    # plt.show()
    return percentile
	


def floor_segmentation_pipeline(file):
	if imwrite_result:
		if not os.path.exists(res_dir):
				os.makedirs(res_dir)

	# Load and resize image 
	img = io.imread(file)  
	imh_orig, imw_orig, _ = img.shape
	img_original = img.copy() # save to aplly mask in the end 
	img = image_resize(img)
	
	gimg = color.rgb2gray(img)
	hsvimg = color.rgb2hsv(img)
	simg = hsvimg[:,:,1]
	vimg = hsvimg[:,:,2]
	write_result(img, 'original')
	write_result(simg, 'saturation')
			
	# SLIC performing
	# img_adapteq = exposure.equalize_adapthist(img, clip_limit=0.5)
	num_segmnts = 300
	labels = slic_segmentation(img, num_segmnts)

	# LBP 
	# edge_map_for_lbp = filters.gaussian(exposure.equalize_adapthist(simg, clip_limit=0.03), sigma=5)
	edges = lbp_filter(img, simg, vimg) # METHOD = 'var' # NaN appears

	# RAG
	# edges = filters.sobel(simg)
	
	# Graph 
	g_edg = graph.rag_boundary(labels, edges)
	write_result(edges, 'edge_map')

	# hierarchical merge
	hierarchical_tresh = weight_histogram(g_edg, 0.80)
	print('======== Treshold is ', hierarchical_tresh)
	labels_hier_slic = graph.merge_hierarchical(labels, g_edg, thresh=hierarchical_tresh, rag_copy=True,
									in_place_merge=True,
									merge_func=merge_boundary,
									weight_func=weight_boundary)

	hierarchical_slic = color.label2rgb(labels_hier_slic, img, kind='avg')
	write_result(hierarchical_slic, 'hierarchical_merged')

	target_label = floor_label(labels_hier_slic)
	mask = np.zeros(labels_hier_slic.shape)
	mask[labels_hier_slic == target_label] = 1

	# reshape mask
	if mask.shape[0] != imh_orig:
		mask = transform.resize(mask, (imh_orig, imw_orig))
		mask[mask <= 0.5] = 0
		mask[mask > 0.5] = 1

	# Mask interpolation	
	# mask = smooth_contour(mask)

	return img_original, mask, hierarchical_tresh


if __name__ == "__main__":

	# Many files
	working_tree = '/home/dzyga/My/Python/ITJim/Projects/FloorSegmentation/'
	dataset_dir = working_tree + 'dataset/'
	gt_dir = dataset_dir + 'ground_truth/'
	res_dir = working_tree + 'result_images/'

	if not os.path.exists(res_dir):
		os.makedirs(res_dir)

	imwrite_result = False
	evaluation = True
	show_stack_photo = False 
	print(' ')

	if evaluation:
		print('Evaluating results wiht ground truth photos')
		iou = []
		# av_precentile = []
		for file in os.listdir(gt_dir):
			if file.endswith('.jpg'):
				res_saveto = res_dir + file[:-5] + '/'
				
				img_gt  = read_ground_truth(gt_dir + file)

				# ---- segmentation function ----
				img, mask, _ = floor_segmentation_pipeline(dataset_dir+file)
				# av_precentile.append(precentile)

				iou_img = iou_evaluation(mask, img_gt)
				print('iou is ', iou_img)
				iou.append(iou_img)
				
				result_img = masking_image(img, mask, img_gt)

				# show results 				
				write_result(result_img, 'result')
				io.imsave(res_dir + file, result_img)
				if show_stack_photo:
					comparing_images(file, result_img)
				print(' ')
			
		meanIoU = np.mean(np.asarray(iou))
		# mean_percentile = np.mean(np.asarray(av_precentile))
		print('Mean IoU is ', meanIoU)
		# print('Mean percentile of 0.035 is ', mean_percentile)

	
	else:
		for file in os.listdir(dataset_dir):
			if file.endswith('.jpg'):
				# res_saveto = res_dir + file[:-5] + '/'
				
				# ---- segmentation function ----
				img, mask, tresh = floor_segmentation_pipeline(dataset_dir+file)
			
				result_img = masking_image(img, mask)
				cv2.putText(result_img, 'thresh is {:.4f}'.format(tresh), (50,100), cv2.FONT_HERSHEY_SIMPLEX, 3, (255,0,0), 4, cv2.LINE_AA)

				# show results 				
				write_result(result_img, 'result')
				io.imsave(res_dir + file, result_img)
				print(' ') 
		

			


